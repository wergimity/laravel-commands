<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\MiddlewareProvider;
use App;
use League\Tactician\CommandBus;

trait ExecuteCommandTrait
{
    /**
     * @param BaseCommand $command
     * @param array       $data
     * @param array       $decorators
     *
     * @return mixed
     */
    protected function execute(BaseCommand $command, array $data = [], array $decorators = [], $validate = true)
    {
        /** @var MiddlewareProvider $provider */
        $provider = App::make(MiddlewareProvider::class);

        if(method_exists($provider, 'init')) {
            $provider->init($data, $decorators, $validate);
        }

        $bus = new CommandBus($provider->middleware());

        return $bus->handle($command);
    }
}