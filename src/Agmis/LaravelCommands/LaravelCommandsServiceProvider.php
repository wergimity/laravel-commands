<?php namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\MiddlewareProvider;
use Agmis\LaravelCommands\Exception\CommandValidationFailed;
use Agmis\LaravelCommands\Middleware\FiresCommandEvents;
use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;

class LaravelCommandsServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MiddlewareProvider::class, CommandMiddlewareProvider::class);

        $this->app->bind('command-bus', function(Container $app) {
            /** @var MiddlewareProvider $provider */
            $provider = $app->make(MiddlewareProvider::class);

            return new CommandBus($provider->middleware());
        }, true);

        $this->app->error(function (CommandValidationFailed $e) {
            return $e->getResponse();
        });
    }

}
