<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\Eventful;

/** @deprecated use \Agmis\LaravelCommands\Contracts\Eventful instead */
interface EventfulCommand extends Eventful
{
}