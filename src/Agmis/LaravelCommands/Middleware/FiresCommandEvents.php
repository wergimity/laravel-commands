<?php
namespace Agmis\LaravelCommands\Middleware;

use Agmis\LaravelCommands\Contracts\Eventful;
use Event;
use League\Tactician\Middleware;

class FiresCommandEvents implements Middleware
{

    /**
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        if(!$command instanceof Eventful) {
            return $next($command);
        }

        Event::fire($command->event() . '.before', [$command]);
        $result = $next($command);
        Event::fire($command->event() . '.after', [$command, $result]);

        return $result;
    }
}