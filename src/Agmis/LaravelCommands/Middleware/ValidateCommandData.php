<?php
namespace Agmis\LaravelCommands\Middleware;

use Agmis\LaravelCommands\Contracts\Validatable;
use Agmis\LaravelCommands\Exception\CommandValidationFailed;
use League\Tactician\Middleware;
use Validator;

class ValidateCommandData implements Middleware
{
    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     * @throws CommandValidationFailed
     */
    public function execute($command, callable $next)
    {
        if(!$command instanceof Validatable || empty($this->data)) {
            return $next($command);
        }

        $messages = $this->messages($command);

        $attributes = $this->attributes($command);

        $validator = Validator::make($this->data, $command->rules(), $messages, $attributes);

        $this->validate($command, $validator);

        return $next($command);
    }

    protected function validate(Validatable $command, $validator)
    {
        if($validator->fails()) {
            throw new CommandValidationFailed($command->fails($validator));
        }
    }

    protected function messages($command)
    {
        if(method_exists($command, 'validatorMessages')) {
            return $command->validatorMessages();
        }

        return [];
    }

    protected function attributes($command)
    {
        if(method_exists($command, 'validatorAttributes')) {
            return $command->validatorAttributes();
        }

        return [];
    }
}