<?php
namespace Agmis\LaravelCommands\Middleware;

use Agmis\LaravelCommands\Contracts\CommandDecorator;
use Agmis\LaravelCommands\Exception\InvalidDecorator;
use App;
use League\Tactician\Middleware;

class CommandDecoratorsChain implements Middleware
{
    /** @var object[]  */
    protected $decorators = [];

    public function __construct(array $decorators = [])
    {
        $this->decorators = $decorators;
    }

    /**
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $chain = $this->createChain();

        $chain($command);

        return $next($command);
    }

    protected function createChain()
    {
        $last = function($command) {};

        /** @var CommandDecorator $decorator */
        while($decorator = array_pop($this->decorators)) {
            $decorator = $this->make($decorator);

            $this->checkInstance($decorator);

            $last = function($command) use ($decorator, $last) {
                return $decorator->execute($command, $last);
            };
        }

        return $last;
    }

    protected function checkInstance($decorator)
    {
        if (!$decorator instanceof CommandDecorator) {
            throw new InvalidDecorator;
        }
    }

    protected function make($decorator)
    {
        if (is_string($decorator)) {
            return App::make($decorator);
        }

        return $decorator;
    }
}