<?php

namespace Agmis\LaravelCommands\Middleware;

use Agmis\LaravelCommands\Contracts\SelfHandling;
use Agmis\LaravelCommands\Exception\CommandHandlerNotFound;
use Illuminate\Container\Container;
use League\Tactician\Middleware;

class ExecuteCommandHandler implements  Middleware
{
    /** @var  Container */
    protected $app;

    /** @var  callable */
    protected $resolver;

    public function __construct(Container $app, callable $resolver)
    {
        $this->app = $app;
        $this->resolver = $resolver;
    }

    /**
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     * @throws CommandHandlerNotFound
     */
    public function execute($command, callable $next)
    {
        if($command instanceof SelfHandling) {
            return $command->handle();
        }

        $handler = $this->handler($command);

        return $handler->handle($command);
    }

    protected function handler($command)
    {
        $handler = call_user_func($this->resolver, $command);

        if(!$handler) {
            throw new CommandHandlerNotFound;
        }

        return $handler;
    }
}