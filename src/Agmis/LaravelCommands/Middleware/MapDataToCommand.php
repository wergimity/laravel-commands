<?php
namespace Agmis\LaravelCommands\Middleware;

use League\Tactician\Middleware;

class MapDataToCommand implements Middleware
{
    /** @var array */
    protected $data = [];

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param object   $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        if (empty($this->data)) {
            return $next($command);
        }

        foreach ($this->data as $key => $value) {
            $this->map($command, $key, $value);
        }

        return $next($command);
    }

    protected function map($command, $key, $value)
    {
        if (!property_exists($command, $key)) {
            return false;
        }

        $command->$key = $value;

        return true;
    }
}