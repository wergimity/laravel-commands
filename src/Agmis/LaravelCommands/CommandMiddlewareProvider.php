<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\CommandDecorator;
use Agmis\LaravelCommands\Contracts\MiddlewareProvider;
use Agmis\LaravelCommands\Middleware\CommandDecoratorsChain;
use Agmis\LaravelCommands\Middleware\ExecuteCommandHandler;
use Agmis\LaravelCommands\Middleware\FiresCommandEvents;
use Agmis\LaravelCommands\Middleware\MapDataToCommand;
use Agmis\LaravelCommands\Middleware\ValidateCommandData;
use Illuminate\Container\Container;

class CommandMiddlewareProvider implements MiddlewareProvider
{
    /** @var Container  */
    protected $app;

    /** @var  array */
    protected $data = [];

    /** @var  CommandDecorator[] */
    protected $decorators = [];

    /** @var  boolean */
    protected $validate = false;

    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    public function init($data = [], $decorators = [], $validate = true)
    {
        $this->data = $data;
        $this->decorators = $decorators;
        $this->validate = $validate;
    }

    public function middleware()
    {
        return [
            new ValidateCommandData($this->validate ? $this->data : []),
            new MapDataToCommand($this->data),
            new CommandDecoratorsChain($this->decorators),
            new FiresCommandEvents(),
            new ExecuteCommandHandler($this->app, [$this, 'resolve'])
        ];
    }

    public function resolve($command)
    {
        $class = get_class($command) . 'Handler';

        if(!class_exists($class)) {
            return null;
        }

        return $this->app->make($class);
    }
}