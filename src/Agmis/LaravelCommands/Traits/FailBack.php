<?php
namespace Agmis\LaravelCommands\Traits;

use Illuminate\Validation\Validator;
use Redirect;
use Request;
use URL;

trait FailBack
{
    public function fails(Validator $validator)
    {
        if (!Request::header('referer')) {
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }
}