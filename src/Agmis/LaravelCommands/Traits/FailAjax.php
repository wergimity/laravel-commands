<?php

namespace Agmis\LaravelCommands\Traits;

use Agmis\LaravelCommands\Exception\CommandValidationFailed;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;

trait FailAjax
{
    /**
     * {@inheritdoc}
     */
    public function fails(Validator $validator)
    {
        throw new CommandValidationFailed(
            Response::create(implode('<br>', $validator->messages()->all()), 500)
        );
    }
}