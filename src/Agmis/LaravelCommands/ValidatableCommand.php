<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\Validatable;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;

/** @deprecated Use \Agmis\LaravelCommands\Contracts\Validatable instead */
interface ValidatableCommand extends Validatable
{

} 