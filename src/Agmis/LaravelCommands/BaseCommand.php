<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\Eventful;
use SimpleBus\Command\Command;
use Str;

abstract class BaseCommand implements Eventful
{
    public function event()
    {
        $reflection = new \ReflectionClass(get_called_class());
        $basename = $reflection->getShortName();
        $result = Str::snake($basename, '.');

        return $result;
    }
}