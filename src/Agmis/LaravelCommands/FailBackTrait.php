<?php
namespace Agmis\LaravelCommands;

use Illuminate\Validation\Validator;
use Redirect;
use Request;
use URL;

/** @deprecated Use Agmis\LaravelCommands\Traits\FailBack instead */
trait FailBackTrait
{
    public function fails(Validator $validator)
    {
        if (!Request::header('referer')) {
            return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }
}