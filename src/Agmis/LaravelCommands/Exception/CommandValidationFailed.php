<?php
namespace Agmis\LaravelCommands\Exception;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CommandValidationFailed extends Exception
{
    protected $response;

    function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}