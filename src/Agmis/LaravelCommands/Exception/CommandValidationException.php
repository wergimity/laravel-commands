<?php
namespace Agmis\LaravelCommands\Exception;

/** @deprecated Use CommandValidationFailed instead */
class CommandValidationException extends CommandValidationFailed
{
}