<?php
namespace Agmis\LaravelCommands\Contracts;

interface MiddlewareProvider
{
    public function middleware();
}