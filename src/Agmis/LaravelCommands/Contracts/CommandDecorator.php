<?php
namespace Agmis\LaravelCommands\Contracts;

interface CommandDecorator
{
    /**
     * Decorates command and executes $next decorator in chain
     *
     * @param          $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next);
}