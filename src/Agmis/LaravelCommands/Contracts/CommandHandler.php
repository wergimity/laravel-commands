<?php
namespace Agmis\LaravelCommands\Contracts;

interface CommandHandler
{
    /**
     * Handles command
     *
     * @param object $command
     *
     * @return mixed
     */
    public function handle($command);
}