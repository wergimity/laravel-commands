<?php
namespace Agmis\LaravelCommands\Contracts;

use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;

interface Validatable
{
    /**
     * Rules used for validation
     *
     * @return array
     */
    public function rules();

    /**
     * What to do when validator fails
     *
     * @param Validator $validator
     *
     * @return Response
     */
    public function fails(Validator $validator);
}