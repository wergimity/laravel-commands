<?php
namespace Agmis\LaravelCommands\Contracts;

interface Eventful
{
    /**
     * Returns event basename
     *
     * @return string
     */
    public function event();
}