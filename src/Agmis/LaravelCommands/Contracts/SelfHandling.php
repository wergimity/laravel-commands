<?php

namespace Agmis\LaravelCommands\Contracts;

interface SelfHandling
{
    public function handle();
}