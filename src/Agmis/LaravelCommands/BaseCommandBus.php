<?php
namespace Agmis\LaravelCommands;

use Agmis\LaravelCommands\Contracts\CommandDecorator;
use SimpleBus\Command\Bus\CommandBus;
use SimpleBus\Command\Command;

/** @deprecated */
abstract class BaseCommandBus implements CommandDecorator
{
    protected $next;

    abstract function handle($command);

    public function execute($command, callable $next)
    {
        $this->next = $next;

        return $this->handle($command);
    }

    protected function next($command)
    {
        $next = $this->next;

        return $next($command);
    }
}