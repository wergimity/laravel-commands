<?php

if(!function_exists('handle')) {
    /**
     * Handles command
     *
     * @param object $command
     *
     * @return mixed
     */
    function handle($command)
    {
        /** @var \League\Tactician\CommandBus $bus */
        $bus = \App::make('command-bus');

        return $bus->handle($command);
    }
}